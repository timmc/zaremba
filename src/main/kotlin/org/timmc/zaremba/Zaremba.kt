package org.timmc.zaremba

import java.math.BigInteger
import kotlin.math.exp
import kotlin.math.ln
import kotlin.math.max
import kotlin.math.pow
import kotlin.math.roundToLong

/**
 * Calculations of z(n) and v(n) and code to search for record-setters.
 */
object Zaremba {
    /**
     * Approximate sigma_1(n), the sum of the divisors, without computing them
     * directly.
     *
     * @param primeFactors The prime factorization of n
     */
    fun sigmaApprox(primeFactors: PrimeExp): Double {
        return primeFactors.mapIndexed { pK, exp ->
            val p = Primes.list[pK].toDouble()
            (p.pow(exp + 1) - 1)/(p - 1)
        }.product()
    }

    /**
     * Approximate h(n) = sigma(n)/n = sum(1/divisor) -- without computing divisors.
     *
     * @param n The number to compute h(n) for
     * @param primeFactors The prime factorization of [n]
     */
    fun hApprox(n: Double, primeFactors: PrimeExp): Double {
        return sigmaApprox(primeFactors) / n
    }

    /**
     * Compute z(n) based on a formula from Weber 2020 [1]. This method avoids
     * having to compute all the divisors of n, which can be computationally much
     * more expensive.
     *
     * [1] https://arxiv.org/pdf/1810.10876.pdf
     *
     * @param n The number to compute z(n) for
     * @param primeFactors The prime factorization of n
     */
    fun z(n: BigInteger, primeFactors: PrimeExp): Double {
        val nApprox = n.toDouble()
        // Precomputing a list of powers of primes did not achieve a noticeable speedup.
        return primeFactors.mapIndexed { pK, exp ->
            val p = Primes.list[pK].toDouble()
            val lnP = ln(p)
            val h = hApprox(nApprox / p.pow(exp), primeFactors.swapAt(pK) { 0 })
            (1..exp).sumOf { j -> j * lnP / p.pow(j) } * h
        }.sum()
    }

    /**
     * Given a prime factorization, compute the divisor count of the number they
     * represent.
     */
    fun primesToTau(primeExponents: PrimeExp): BigInteger {
        return primeExponents.map { BigInteger.ONE + it.toBigInteger() }.product()
    }

    /**
     * Given a primorial factorization, compute the divisor count of the number
     * they represent.
     */
    fun primorialsToTau(primorialExponents: PrimorialExp): BigInteger {
        return primesToTau(Primorials.toPrimes(primorialExponents))
    }


    //================//
    // Regular search //
    //================//

    /**
     * A record-setting value for z(n), v(n), or both.
     */
    data class RecordSetter(
        /**
         * The value n that is a record-setter for v or z or both.
         *
         * This is a String because JSON can't handle round-trip large integers.
         */
        val n: String, // JSON doesn't handle BigInteger well
        /**
         * The value tau(n) for [n], the count of its divisors.
         *
         * This is again a BigInteger that has been stringified.
         */
        val tau: String,
        /** The value z(n) for [n]. */
        val z: Double,
        /** Whether this z(n) was a record-setter. */
        val isZRecord: Boolean,
        /** The value v(n) for [n]. */
        val v: Double,
        /** Whether this v(n) was a record-setter. */
        val isVRecord: Boolean,
        /** Prime factorization of [n]. */
        val primes: PrimeExp,
        /** Primorial factorization of [n]. */
        val primorials: PrimorialExp,
    )

    /**
     * Yield all n >= 4 that produce record-setting values for z(n) or v(n).
     *
     * This starts at n=4 because of issues with small n:
     *
     * - n=1 produces NaN for v
     * - n=2 is fine, but...
     * - n=3 is not a waterfall number but *would* be a record-setter,
     *   and so there would be a gap. (It's known to be the only such exception.)
     *
     * Starting at n=4 allows us to avoid these issues.
     */
    fun findRecords(maxN: BigInteger): Sequence<RecordSetter> {
        return sequence {
            var recordZ = -1.0
            var recordV = -1.0

            for (n in Waterfall.findUpTo(maxN).drop(2)) {
                val primeExp = Primorials.toPrimes(n.primorialExponents)
                val tau = primesToTau(primeExp)
                val z = z(n.value, primeExp)
                val v = z / ln(tau.toDouble())

                val isRecordZ = z > recordZ
                val isRecordV = v > recordV

                if (isRecordZ || isRecordV) {
                    yield(RecordSetter(
                        n = n.value.toString(), tau = tau.toString(),
                        z = z, isZRecord = isRecordZ,
                        v = v, isVRecord = isRecordV,
                        primes = primeExp, primorials = n.primorialExponents,
                    ))
                }

                recordZ = max(z, recordZ)
                recordV = max(v, recordV)
            }
        }
    }


    //==========//
    // K-Primes //
    //==========//

    /**
     * Intermediate calculation values from a call to [searchVRecordKPrimes].
     *
     * These are used in the halting condition of [maxVByBootstrapping] but are
     * also used for testing and debugging.
     */
    data class KPrimesIntermediate(
        /** An upper bound on z(n). */
        val zMax: Double,
        /** A lower bound on tau(n). */
        val tauMin: BigInteger,
        /** An upper bound on tau(n). */
        val tauMax: BigInteger,
        /** Natural log of [tauMax]. */
        val logTauMax: Double,
    )

    /**
     * A candidate produced by [searchVRecordKPrimes], which may or may not be
     * a record-setter but conforms to the parameters of the call.
     *
     * [n] is the candidate, and is accompanied by its decomposition into
     * primes and primorials, as well as tau, z, and v computations.
     */
    data class KPrimesResult(
        val n: BigInteger,
        val primorials: PrimorialExp,
        val primes: PrimeExp,
        val tau: BigInteger,
        val z: Double,
        val v: Double,
    )

    /**
     * Compute v(n) for all candidate waterfall numbers with k primes, informed
     * by a previous record-setting v(n) value.
     *
     * By calling this with successively larger values of k, until some halting
     * condition is reached, we can search for all possible higher v(n) values
     * exhaustively.
     *
     * @param k Only produce candidate waterfall numbers that have exactly this
     *   many distinct primes.
     * @param vRecord Some previous record-setting value of v(n), used to limit
     *   the tau of candidate waterfall numbers
     * @return A pair of 1) intermediate calculations, and 2) all candidate
     *   waterfall numbers and their z, tau, and v values. These candidates will
     *   need to be filtered, sorted, and scanned to see if any are actually new
     *   record-setters.
     */
    fun searchVRecordKPrimes(
        k: Int, vRecord: Double
    ): Pair<KPrimesIntermediate, Sequence<KPrimesResult>> {
        // We know that there's an existing record-setter V_a (here, vRecord.)
        // We're seeking a larger one, V_b > V_a. We then posit that in
        // v(n) = V_b, n has a certain number of distinct primes, k.
        //
        // Given k, we can find an upper bound on the z(n) that would be part of
        // V_b's calculation (z/log tau). The maximum value of V_b is limited by
        // the maximum value of this z(n).
        //
        // So, our first step is to find this value, which we'll call zMax:

        // Get the first k primes, as doubles
        val kPrimesD = Primes.list.take(k).map(BigInteger::toDouble)
        // We can get an upper bound on z(n) using Weber's lemma. We have to
        // assume that any and all of the first k primes could be factors, so
        // instead of being able to check if p|n, we just use all of p_1 through
        // p_k. (More below on why we err in this direction.)
        val zMax = kPrimesD.map { p -> p/(p - 1.0) }.product() *
            kPrimesD.sumOf { p -> ln(p)/(p - 1.0) }

        // The other part of the calculation is log(tau(n)). For a given k,
        // there are an infinite number of possible waterfall numbers, and
        // values for tau. We cannot enumerate them.
        //
        // However, as tau (really log(tau), but same logic) increases in the
        // denominator, v(n) decreases. At some point we would reach the
        // break-even point of v(n) = V_a, after which we would no longer be able
        // to set a new record.
        //
        // The larger z(n) is, the larger tau can rise without hitting the
        // break-even, so it's beneficial to err on the side of a larger z(n).
        // That's what allowed us to use *all* the primes through p_k in the
        // zMax calculation above.
        //
        // So the break-even point is V_a = zMax/log(tau); solve for tau to get
        // tauMax. Any new record-setter must have a tau no larger than this.
        val logTauMax = zMax/vRecord
        val tauMax = exp(logTauMax).roundToLong().toBigInteger()

        // It's also useful to know what the *smallest* tau could be, since this
        // tells us what the *largest* v could be. If tauMin is too large then
        // it probably isn't worth looking at higher primes.
        val tauMin = primesToTau(List(k) { 1 })

        // Gather up these intermediate values for printing or iterating
        val intermediate = KPrimesIntermediate(zMax, tauMin, tauMax, logTauMax)

        // Now we can generate all waterfall numbers n (and their v(n)) with
        // tau(n) ≤ tauMax. This is a relatively tractable set to enumerate.
        val results = Waterfall.forKPrimesAndMaxTau(k, tauMax).map { n ->
            val primeExponents = Primorials.toPrimes(n.primorialExponents)
            val tau = primesToTau(primeExponents)
            val z = z(n.value, primeExponents)
            val v = z / ln(tau.toDouble())
            KPrimesResult(
                n = n.value,
                primorials = n.primorialExponents, primes = primeExponents,
                tau = tau, z = z, v = v,
            )
        }

        return intermediate to results
    }

    /**
     * Union type for [maxVByBootstrapping] output. This allows log messages to
     * be provided to the caller so that they can be printed, but only if
     * desired.
     */
    sealed class BootstrappingOutput
    /** Log message from [maxVByBootstrapping] */
    data class BootstrapMessage(val msg: String): BootstrappingOutput()
    /** Actual record-setter result from [maxVByBootstrapping] */
    data class BootstrapRecord(val v: Double): BootstrappingOutput()

    /**
     * Find some subset of record-setters for v(n), including the largest.
     *
     * This uses a "reverse bootstrapping" technique. We generate and examine
     * all candidate waterfall numbers composed of exactly k distinct primes
     * and a tau limited by the previous v(n) record. k is incremented until a
     * a new record is found, at which point the process starts over from k=1
     * again. This continues until k=54 is reached, which is a loose upper
     * bound on the number of primes in n.
     *
     * Taking the `.last()` of the returned sequence and passing it as maxN to
     * [findRecords], followed by a filtering step, will allow you to obtain the
     * complete list of v(n) record-setters.
     *
     * @return An intermixed stream of progress messages and various
     *   record-setters for v(n). This is an incomplete subset of
     *   record-setters, but the largest will be present (and last).
     */
    fun maxVByBootstrapping(): Sequence<BootstrappingOutput> {
        // This is a weak upper bound on the number of primes that a
        // record-setting n can have for v(n). This has been worked out by hand
        // to a tighter limit of k=29, but the calculations are fast enough that
        // it's no trouble to use the looser bound.
        val maxK = 54

        return sequence {
            // Start with known value for v(4)
            var vRecord = 0.6309297535714574
            yield(BootstrapMessage("Starting bootstrap with v(4) = $vRecord"))

            // Get higher and higher V values until we stop finding them
            while (true) {
                // For the current best record, try higher and higher prime
                // counts k until they are incapable of producing a new record
                // (or until a new record is found).
                var k = 1
                while (true) {
                    yield(BootstrapMessage("  Searching for next record with $k primes"))
                    val (intermediate, candidates) = searchVRecordKPrimes(k, vRecord)

                    // We could just stop at the first new record-setter, but
                    // might as well sort and find the highest one in these results.
                    val newRecord = candidates.filter { it.v > vRecord }.sortedBy { it.v }.lastOrNull()
                    yield(BootstrapMessage("    Checked ${candidates.count()} candidates, with max tau = ${intermediate.tauMax}"))

                    if (newRecord != null) {
                        yield(BootstrapMessage("    Found new record! v=${newRecord.v}\tn=${newRecord.n}\tz=${newRecord.z}\ttau=${newRecord.tau}"))
                        vRecord = newRecord.v
                        yield(BootstrapRecord(newRecord.v))
                        break
                    }

                    // We didn't find a result on this k, so check to see if
                    // we've gone beyond the known upper bound on k.
                    if (k >= maxK) {
                        yield(BootstrapMessage("  Stopping: No record-setters above k=$maxK"))
                        yield(BootstrapMessage("Greatest v(n) = $vRecord"))
                        return@sequence
                    }

                    k++
                }
            }
        }
    }
}
