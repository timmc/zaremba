package org.timmc.zaremba

import java.math.BigInteger

/**
 * A waterfall number, represented both as its numeric value and as its unique
 * decomposition into primorial factors.
 *
 * These are https://oeis.org/A025487 and are also known as Hardy-Ramanujan
 * numbers. Different ways of looking at a waterfall number:
 *
 * - Product of zero or more primorials, including repeats
 * - Prime factorization starts with 2, has no gaps in the list of prime
 *   factors, and has non-ascending exponents on the primes.
 *
 * More formally: Where p_i are the consecutive primes starting with p_1 = 2,
 * and n has prime factorization `p_1^a_1 * p_2^a_2 * ... * p_k^a_k`, and if
 * `a_1 >= a_2 >= a_3 >= ... >= a_k`, then n is a waterfall number.
 *
 * For example, `10080 = (2^5)(3^2)(5^1)(7^1)`.
 */
data class WaterfallNumber(
    /** The value of this waterfall number. */
    val value: BigInteger,

    /**
     * Decomposition of the number into primorials. See [PrimorialExp] for
     * details.
     */
    val primorialExponents: PrimorialExp,
)

/**
 * Utility to find waterfall numbers.
 */
object Waterfall {
    /**
     * Throw if a list of prime exponents does not represent a waterfall number.
     */
    fun assertWaterfall(exps: PrimeExp) {
        if (!exps.zipWithNext { a, b -> a >= b }.all { it }) {
            throw AssertionError("Prime exponents failed waterfall test: $exps")
        }
    }

    /**
     * Recursive core of [findUpTo]. Given a base value (as both a waterfall
     * number and its primorial decomposition), yield waterfall numbers that
     * are multiples of that base
     *
     * Explores in two directions:
     *
     * - All possible powers of the current primorial (that is, the first value
     *   in [primorials])
     * - The next primorial, as a self-recursive call (one for each power of the
     *   current primorial, including exponent = 0)
     *
     * @param baseProduct The base waterfall number that we're exploring from.
     * @param baseExp The primorial decomposition of [baseProduct] (possibly
     *   ending in zeroes, since this is an intermediate value.)
     * @param primorials Primorials that have not yet been explored on this
     *   branch. This is Primorials.list after the first `baseExp.length`
     *   elements have been removed.
     * @param maxN The largest waterfall number N that will be computed. This
     *   serves to truncate the recursive search.
     * @return Unsorted sequence of waterfall numbers that are multiples of the
     *   base. This will *not* include the base value, which the caller must
     *   separately ensure is accounted for.
     */
    private fun findUpToFrom(
        baseProduct: BigInteger, baseExp: PrimorialExp,
        primorials: List<BigInteger>, maxN: BigInteger,
    ): Sequence<WaterfallNumber> {
        return sequence {
            // If we ever reach the end of the list *before* we find a primorial
            // that is larger than N, raise an exception -- we need a larger list
            // of prime numbers!
            if (primorials.isEmpty())
                throw AssertionError("Ran out of primorials")

            val factor = primorials[0]
            if (factor > maxN)
                return@sequence

            // TODO: Check if there's a worthwhile performance gain in using
            //   indexes instead of a list that requires repeated modification.
            val nextPrimorials = primorials.drop(1)
            // It's tempting to deduplicate this recursive call with the one in
            // the loop but it seems difficult to do so in a way that is
            // correct, clear, and preserves loop invariant logic.
            yieldAll(findUpToFrom(baseProduct, baseExp.plus(0), nextPrimorials, maxN))

            var nextProduct = baseProduct
            var exponent = 0

            while (true) {
                nextProduct = factor * nextProduct
                if (nextProduct > maxN)
                    break
                exponent += 1

                val nextExp = baseExp.plus(exponent)
                yield(WaterfallNumber(
                    value = nextProduct,
                    primorialExponents = nextExp
                ))
                yieldAll(findUpToFrom(nextProduct, nextExp, nextPrimorials, maxN))
            }
        }
    }

    /**
     * Produce a sorted sequence of all the waterfall numbers up to N, starting with
     * 1, along with their factorization as primorial exponents.
     */
    fun findUpTo(maxN: BigInteger): Sequence<WaterfallNumber> {
        val all = findUpToFrom(BigInteger.ONE, emptyList(), Primorials.list, maxN)
            .plus(WaterfallNumber(value = BigInteger.ONE, primorialExponents = emptyList()))
        return all.sortedBy(WaterfallNumber::value)
    }

    /**
     * Recursive core of [forKPrimesAndMaxTau].
     *
     * Given a base waterfall number (as a primorial decomposition), yield
     * waterfall numbers that are multiples of that base by incrementing
     * exponents at various indexes (from [expIndex] up to the last index).
     *
     * Explores in two directions:
     *
     * - All possible exponent values for the current index [expIndex]
     * - The next index, as a self-recursive call (one for each exponent value
     *   tried in the current index, including 0.)
     *
     * @param base The waterfall number to start from, as a primorial
     *   decomposition of a fixed length (may have trailing zeroes, as this is
     *   an intermediate value.)
     * @param expIndex The index into [base] that we'll try incrementing in
     *   order to find larger multiples.
     * @param maxTau Stop exploring in the current direction if it would produce
     *   numbers with a tau(n) greater than this.
     * @return Unsorted sequence of waterfall numbers that are multiples of the
     *   base. This will *not* include the base value, which the caller must
     *   separately ensure is accounted for.
     */
    private fun forKPrimesAndMaxTauFrom(
        base: PrimorialExp, expIndex: Int, maxTau: BigInteger,
    ): Sequence<WaterfallNumber> {
        return sequence {
            val canExploreRightwards = expIndex + 1 < base.size
            var curExp = base
            while (true) {
                // Explore rightwards from each variation on this position
                if (canExploreRightwards) {
                    yieldAll(forKPrimesAndMaxTauFrom(curExp, expIndex + 1, maxTau))
                }

                // Explore further upwards
                curExp = curExp.swapAt(expIndex) { it + 1 }

                val curTau = Zaremba.primorialsToTau(curExp)
                if (curTau > maxTau)
                    break

                // Composition has to be done here instead of keeping a running
                // product, unlike in [findUpToFrom].
                yield(WaterfallNumber(
                    value = Primorials.unfactor(curExp),
                    primorialExponents = curExp,
                ))
            }
        }
    }

    /**
     * Produce all waterfall numbers that use exactly the first k primes, with
     * a maximum tau value of the product.
     */
    fun forKPrimesAndMaxTau(kPrimes: Int, maxTau: BigInteger): Sequence<WaterfallNumber> {
        // Start with nothing except the largest primorial, raised to 1. This is
        // the same as having the prime exponents all set to 1.
        val startingExponents: PrimorialExp = List(kPrimes - 1) { 0 } + listOf(1)
        return sequence {
            // Manually yield first value, since recursive function won't
            if (Zaremba.primorialsToTau(startingExponents) <= maxTau) {
                yield(WaterfallNumber(
                    Primorials.unfactor(startingExponents),
                    startingExponents,
                ))
            }

            yieldAll(forKPrimesAndMaxTauFrom(startingExponents, 0, maxTau))
        }
    }

    /**
     * Produce the prime factorization of a waterfall number.
     *
     * TODO: Switch to factoring into primorials instead? Not needed for z(n)
     *   calculation any more, but would be faster if that's needed at some
     *   point.
     *
     * @return a map of prime factors to their counts, or null if not a waterfall
     *   number
     */
    fun factor(n: BigInteger): PrimeExp? {
        // Running remainder, and factors so far
        var remainder = n
        val factors = mutableListOf<Int>()
        var previousPrimeRepeat = Int.MAX_VALUE // used for waterfall checks
        var factored = false

        for (prime in Primes.list) {
            // Keep dividing n by prime until we can't
            var repeats = 0
            while (remainder.mod(prime) == BigInteger.ZERO) {
                remainder /= prime
                repeats++
            }

            // Check if this fails the "non-ascending" constraint
            if (repeats > previousPrimeRepeat)
                return null

            if (repeats == 0) {
                // This prime wasn't a factor at all. Check what that means:
                if (remainder == BigInteger.ONE) {
                    factored = true
                    break
                } else {
                    // Violates the "contiguous" constraint.
                    return null
                }
            } else {
                factors.add(repeats)
            }

            previousPrimeRepeat = repeats
        }

        if (!factored)
            throw AssertionError("Ran out of primes when factoring")

        return factors.toList()
    }
}
