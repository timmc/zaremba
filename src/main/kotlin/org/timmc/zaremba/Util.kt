package org.timmc.zaremba

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import java.math.BigInteger

object Util {
    /** JSON reader/writer and data mapper. */
    val moshi: Moshi = Moshi.Builder()
        .addLast(KotlinJsonAdapterFactory())
        .build()
}

/**
 * Replace the value at index [at] by transforming it with [mapper].
 *
 * @return a new list with the function result placed into the specified index
 */
fun <T> List<T>.swapAt(at: Int, mapper: (T) -> T): List<T> {
    if (!indices.contains(at))
        throw IndexOutOfBoundsException("Cannot swap value at out-of-bounds index $at")
    return mapIndexed { i, v -> if (i == at) mapper(v) else v }
}

/** Compute product of a list of Double. */
fun Iterable<Double>.product(): Double = fold(1.0, Double::times)

/** Compute product of a list of Long, throwing if overflow occurs. */
fun Iterable<Long>.product(): Long = fold(1L, Math::multiplyExact)

/** Compute product of a list of BigInteger. */
fun Iterable<BigInteger>.product(): BigInteger = fold(BigInteger.ONE, BigInteger::times)
