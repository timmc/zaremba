# Zaremba calculations

Zaremba function record-setting calculations, written in Kotlin.

This code supports the preprint [Weighted Versions of the Arithmetic-Mean-Geometric Mean Inequality and Zaremba's Function](https://arxiv.org/abs/2312.11661).

## Build

Requires Maven and JDK 11+.

```bash
mvn package
```

## Example calls

Outputs for these are in the `output` directory.

### All z(n) and v(n) records up to a maximum n

Calculate records for z(n) and v(n) from n=4 up to n=1e40 (expect this to take
up to 3 minutes and 4 GB of memory):

```bash
java -jar target/zaremba-*-jar-with-dependencies.jar records 10000000000000000000000000000000000000000 > output/records.json
```

Reformat records for LaTeX table output:

```bash
java -jar target/zaremba-*-jar-with-dependencies.jar latex output/records.json > output/records.latex
```

### Search for maximum v(n)

Use k-primes/max-tau approach instead, with highest known v(n) record
as input to see if there are any larger ones (~25 seconds):

```bash
for k in {1..35}; do echo "Running with k=$k"; java -jar target/zaremba-*-jar-with-dependencies.jar k-primes --V 1.7059578102443238 --k $k; echo; echo; done | tee output/k-primes-multi.txt
```

Fully automatic bootstrapping of finding the highest v(n), using
k-primes (~4 seconds):

```bash
java -jar target/zaremba-*-jar-with-dependencies.jar max-v | tee output/max-v.txt
```

## Possible improvements

- Parallelize waterfall number search (not sure if worth the effort)
- Figure out a scalable approach to waterfall search continuation
  points so that we can reach waterfall numbers beyond 1e43 or so (see
  `continuation` and `waterfall-iterbatch` branches for failed
  attempts)
- Automated tests for checking bounds on floating point error (maybe
  can detect if a record would be within the margin of error), perhaps
  by randomizing operation order
